#ifdef CAR_H
#define CAR_H
#include <string.h>

class Car {

    private: 
        std::string_model;
        int year;
        double speed;
        bool engine_on;

    public:
        Car();
        Car(std::string_model, int year, bool engine_on);
        ~Car()
        std::string get_string_model();
        int get_year();
        double get_speed();

        void set_speed(double speed);
        
        void start_engine();
        void stop_engine();
        void accelerate(double amount);
        void decelerate(double amount);
}   

#endif