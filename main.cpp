#include <iostream>
#include "car.h"
#include "car.cpp"

using namespace std;

int main(int argc, char** argv){

    Car c1;
    
    c1{
        "Ford", 2012
    };
    c1.get_year();
	
	return 0;
}